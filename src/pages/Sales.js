import {
  Box,
  Container,
  Typography
} from '@material-ui/core';

const Sales = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3,
      }}
    >
      <Container maxWidth={false}>
        <Typography>Sales</Typography>
      </Container>
    </Box>
  </>
);

export default Sales;
