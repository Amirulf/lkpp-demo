import {
  Box,
  Container,
  Grid,
} from '@material-ui/core';
import Report from '../components/report/Report';

const Reporting = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3,
      }}
    >
      <Container maxWidth={false}>
        <Grid container spacing={3}>
          <Grid item lg={12} md={12} xs={12} pt={3}>
            <Report />
          </Grid>
        </Grid>
      </Container>
    </Box>
  </>
);

export default Reporting;
