import {
  Box,
  Container,
} from '@material-ui/core';
import Admin from '../components/admin/Admin';

const Customer = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Admin />
      </Container>
    </Box>
  </>
);

export default Customer;
