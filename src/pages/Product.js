import {
  Box,
  Container,
  Grid
} from '@material-ui/core';

import ProductList from '../components/product/product';

const Product = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3,
      }}
    >
      <Container maxWidth={false}>
        <Grid item lg={12} md={12} xs={12} pt={3}>
          <ProductList />
        </Grid>
      </Container>
    </Box>
  </>
);
export default Product;
