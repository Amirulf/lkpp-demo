import {
  Box,
  Container,
  Typography
} from '@material-ui/core';
import DownloadData from '../components/download/Product';

const Dashboard = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Typography>Dashboard</Typography>
        <DownloadData />
      </Container>
    </Box>
  </>
);

export default Dashboard;
