import axios from 'axios';

const header = { Authorization: localStorage.getItem('Authorization') };

export async function getAllProduct() {
  const product = await axios.get('https://research.crocodic.net/demo_spse/goods/public/api/list-data');
  return product;
}
export async function getProductById(id) {
  const product = await axios.get(`https://research.crocodic.net/demo_spse/goods/public/api/detail?sku=${id}`);
  return product;
}
export async function productCreate(values) {
  const product = await axios.post('https://research.crocodic.net/demo_spse/goods/public/api/create', values);
  return product;
}
export async function productEdit(values) {
  const product = await axios.post('https://research.crocodic.net/demo_spse/goods/public/api/update', values);
  return product;
}
export async function productDelete(values) {
  const product = await axios.post('https://research.crocodic.net/demo_spse/goods/public/api/delete', values);
  return product;
}
export async function createReport(values) {
  const report = await axios.post('https://research.crocodic.net/demo_spse/report/public/api/reports/create', values);
  return report;
}
export async function getReportByCustomerId() {
  const report = await axios.get('https://research.crocodic.net/demo_spse/report/public/api/reports?customer_id=1');
  return report;
}
export async function registerUser(values) {
  const report = await axios.post('http://103.20.184.59:3000/users/register-user', values);
  return report;
}
export async function loginUser(values) {
  const report = await axios.post('http://103.20.184.59:3000/users/login-user', values);
  localStorage.setItem('Authorization', report.data.token);
  window.location.replace('/#/app/sales');
  return report;
}
export async function registerAdmin(values) {
  const report = await axios.post('http://103.20.184.59:3000/users/register-admin', values);
  return report;
}
export async function addAmin(values) {
  const admin = await axios.post('http://103.20.184.59:3000/users/register-admin', values);
  if (admin) {
    window.location.reload();
  }
}
export async function loginAdmin(values) {
  const report = await axios.post('http://103.20.184.59:3000/users/login-admin', values);
  localStorage.setItem('Authorization', report.data.token);
  window.location.replace('/#/app/dashboard');
  return report;
}
export async function getAllAdmin() {
  const admin = await axios.get('http://103.20.184.59:3000/users/user/all', { headers: header });
  return admin;
}
export async function deleteAdmin(id) {
  const admin = await axios.delete(`http://103.20.184.59:3000/users/user/${id}`, { headers: header });
  if (admin) {
    window.location.reload();
  }
}
