import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { addAmin } from '../../utils/promiseMethod';

const AddForm = () => {
  const validation = Yup.object().shape({
    firstName: Yup.string().max(255).required('Firstname is required'),
    lastName: Yup.string().max(255).required('Lastname is required'),
    city: Yup.string().max(255).required('City is required'),
    phone: Yup.string().max(255).required('phone is required'),
    email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
    password: Yup.string().max(255).required('Password is required')
  });

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        city: '',
        email: '',
        phone: '',
        password: ''
      }}
      validationSchema={validation}
      onSubmit={(values) => {
        addAmin(values);
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
      }) => (
        <form onSubmit={handleSubmit}>
          <Card>
            <CardHeader title="Add Admin" />
            <Divider />
            <CardContent>
              <div>
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.firstName && errors.firstName)}
                  helperText={touched.firstName && errors.firstName}
                  label="Firstname"
                  margin="normal"
                  name="firstName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="name"
                  value={values.firstName}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.lastName && errors.lastName)}
                  helperText={touched.lastName && errors.lastName}
                  label="Lastname"
                  margin="normal"
                  name="lastName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="name"
                  value={values.lastName}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.city && errors.city)}
                  helperText={touched.city && errors.city}
                  label="City"
                  margin="normal"
                  name="city"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="name"
                  value={values.city}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.phone && errors.phone)}
                  helperText={touched.phone && errors.phone}
                  label="Phone"
                  margin="normal"
                  name="phone"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="phone"
                  value={values.phone}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.email && errors.email)}
                  helperText={touched.email && errors.email}
                  label="Email Address"
                  margin="normal"
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="email"
                  value={values.email}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.password && errors.password)}
                  helperText={touched.password && errors.password}
                  label="Password"
                  margin="normal"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="password"
                  value={values.password}
                  variant="outlined"
                />
                <Button
                  style={{ margin: 15, padding: 15, }}
                  color="primary"
                  disabled={isSubmitting}
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Add Admin
                </Button>
              </div>
            </CardContent>
            <Divider />
          </Card>
        </form>
      )}
    </Formik>
  );
};

export default AddForm;
