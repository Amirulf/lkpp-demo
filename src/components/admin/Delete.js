import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from '@material-ui/core';
import { deleteAdmin } from '../../utils/promiseMethod';

/* eslint no-underscore-dangle: 0 */
const Delete = ({ open, onClose, users }) => (
  <div>
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`Are you sure to delete 
          ${users.firstName}${users.lastName} 
          ?`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          cancel
        </Button>
        <Button onClick={() => deleteAdmin(users._id)} color="primary" autoFocus>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);

Delete.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  users: PropTypes.func.isRequired
};
export default Delete;
