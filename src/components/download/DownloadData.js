import React from 'react';
import PropTypes from 'prop-types';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Button } from '@material-ui/core';

const DownloadData = ({ apiData, fileName }) => {
  console.log(apiData);
  const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  const fileExtension = '.xlsx';
  const exportToCSV = () => {
    const ws = XLSX.utils.json_to_sheet(apiData);
    const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
  };
  return (
    <Button color="primary" variant="contained" onClick={() => exportToCSV(apiData, fileName)}>Export Product To xlsx</Button>
  );
};
DownloadData.propTypes = {
  apiData: PropTypes.array.isRequired,
  fileName: PropTypes.string.isRequired
};
export default DownloadData;
