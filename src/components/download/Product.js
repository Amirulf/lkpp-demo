import { useState, useEffect } from 'react';
import { getAllProduct } from '../../utils/promiseMethod';
import DownloadData from './DownloadData';

const Product = () => {
  const [items, setItems] = useState([]);
  const fileName = 'myfile';

  useEffect(async () => {
    const result = await getAllProduct();
    setItems(result.data.data);
  }, []);

  return (
    <DownloadData apiData={items} fileName={fileName} />
  );
};

export default Product;
