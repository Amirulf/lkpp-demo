import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  AppBar,
  Box,
  Hidden,
  IconButton,
  Toolbar
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import InputIcon from '@material-ui/icons/Input';

const DashboardNavbar = ({ onMobileNavOpen, ...rest }) => {
  const navigate = useNavigate();
  const logOut = () => {
    localStorage.removeItem('Authorization');
    navigate('/login', { replace: true });
  };

  return (
    <AppBar
      color="transparent"
      elevation={0}
      {...rest}
    >
      <Toolbar>
        <Box sx={{ flexGrow: 1 }} />
        <Hidden lgDown>
          <IconButton onClick={() => logOut()} color="primary">
            <InputIcon label="Logout" />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onMobileNavOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

DashboardNavbar.propTypes = {
  onMobileNavOpen: PropTypes.func
};

export default DashboardNavbar;
