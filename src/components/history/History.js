import { useState, useEffect } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  LinearProgress
} from '@material-ui/core';
import { getReportByCustomerId } from '../../utils/promiseMethod';

const ReportHistory = ({ ...rest }) => {
  const [items, setItems] = useState([]);
  const [limit, setLimit] = useState(10);
  const [rowPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [isLoading, setLoading] = useState(true);

  useEffect(async () => {
    const result = await getReportByCustomerId();
    setItems(result.data.data);
    setLoading(false);
  }, []);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowPerPage - Math.min(rowPerPage, items.length - page * rowPerPage);
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Box>
      {isLoading ? (
        <LinearProgress />
      ) : (
        <Card {...rest}>
          <PerfectScrollbar>
            <Box sx={{ minWidth: 1050 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Sales</TableCell>
                    <TableCell>Product</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Total</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowPerPage > 0
                    ? items.slice(page * rowPerPage, page * rowPerPage + rowPerPage)
                    : items.data
                  ).map((item) => item.items.map((report) => (
                    /* eslint no-underscore-dangle: 0 */
                    <TableRow hover key={item.id}>
                      <TableCell>{item.customer_name}</TableCell>
                      <TableCell>{item.sales_code}</TableCell>
                      <TableCell>{report.goods_name}</TableCell>
                      <TableCell>{report.goods_amount}</TableCell>
                      <TableCell>
                        {Intl.NumberFormat('id-ID', {
                          style: 'currency',
                          currency: 'IDR',
                          maximumSignificantDigits: 3
                        }).format(report.goods_price)}
                      </TableCell>
                      <TableCell>
                        {Intl.NumberFormat('id-ID', {
                          style: 'currency',
                          currency: 'IDR',
                          maximumSignificantDigits: 3
                        }).format(report.goods_total)}
                      </TableCell>
                    </TableRow>
                  )))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <TablePagination
            component="div"
            count={items.length}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[10, 15, 25]}
          />
        </Card>
      )}
    </Box>
  );
};

export default ReportHistory;
