import { useState, useEffect, useRef } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Avatar,
  LinearProgress,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import DetailsIcon from '@material-ui/icons/Details';
import { getAllProduct, getProductById } from '../../utils/promiseMethod';
import Detail from './Detail';
import Edit from './Edit';
import Add from './Add';
import Delete from './Delete';

const Product = ({ ...rest }) => {
  const myRef = useRef(null);
  const [items, setItems] = useState([]);
  const [limit, setLimit] = useState(10);
  const [rowPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [open, setOpen] = useState(false);
  const [deletes, setDelete] = useState(false);
  const [show, setShow] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [productDetail, setProductDetail] = useState('');

  useEffect(async () => {
    const result = await getAllProduct();
    setItems(result.data.data);
    setLoading(false);
  }, []);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowPerPage - Math.min(rowPerPage, items.length - page * rowPerPage);
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  // modal Detail;
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async (item) => {
    setOpen(true);
    setProductDetail(item);
    console.log(productDetail);
  };

  // modal delete;
  const handleCloseDelete = () => {
    setDelete(false);
  };
  const handleDelete = (item) => {
    setDelete(true);
    setProductDetail(item);
  };

  // form add and edit;
  const handleHide = () => {
    setShow(false);
  };
  const handleShow = async (item) => {
    setShow(true);
    const result = await getProductById(item.sku);
    myRef.current.scrollIntoView();
    setProductDetail(result.data.data);
  };

  return (
    <Box style={{ marginTop: -50 }}>
      <div ref={myRef}>
        {!show ? (
          <>
            <Add />
            <br />
          </>
        ) : (
          <>
            <Edit products={productDetail} handleCancel={() => handleHide()} />
            <br />
          </>
        )}
      </div>
      {isLoading ? (
        <LinearProgress />
      ) : (
        <Card {...rest}>
          <PerfectScrollbar>
            <Box sx={{ minWidth: 1050 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Image</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Sku</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Stock</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowPerPage > 0
                    ? items.slice(page * rowPerPage, page * rowPerPage + rowPerPage)
                    : items.data
                  ).map((item) => (
                    /* eslint no-underscore-dangle: 0 */
                    <TableRow hover key={item.id}>
                      <TableCell>
                        <Avatar
                          alt="Image"
                          src={item.image}
                          sx={{ width: 70, height: 70 }}
                        />
                      </TableCell>
                      <TableCell>{item.name}</TableCell>
                      <TableCell>{item.sku}</TableCell>
                      <TableCell>{item.description}</TableCell>
                      <TableCell>
                        {Intl.NumberFormat('id-ID', {
                          style: 'currency',
                          currency: 'IDR',
                          maximumSignificantDigits: 3
                        }).format(item.price)}
                      </TableCell>
                      <TableCell>{`${item.stock} Pcs`}</TableCell>
                      <TableCell align="center">
                        <Button
                          onClick={() => handleOpen(item)}
                          variant="contained"
                          style={{ margin: 3 }}
                        >
                          <DetailsIcon />
                          <small>Detail</small>
                        </Button>
                        <Button
                          variant="contained"
                          onClick={() => handleShow(item)}
                          style={{ margin: 3 }}
                          disabled={show}
                        >
                          <EditIcon />
                          <small>Edit</small>
                        </Button>
                        <Button
                          onClick={() => handleDelete(item)}
                          variant="contained"
                          color="secondary"
                          style={{ margin: 3 }}
                        >
                          <DeleteIcon />
                          <small>Delete</small>
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <Detail
            open={open}
            itemDetails={productDetail}
            onClose={handleClose}
          />
          <Delete
            open={deletes}
            products={productDetail}
            onClose={handleCloseDelete}
          />
          <TablePagination
            component="div"
            count={items.length}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[10, 15, 25]}
          />
        </Card>
      )}
    </Box>
  );
};

export default Product;
