import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { productCreate } from '../../utils/promiseMethod';

const AddForm = () => {
  const validation = Yup.object().shape({
    name: Yup.string().max(255).required('Name is required!'),
    description: Yup.string().max(255).required('Description is required!'),
    price: Yup.string().max(255).required('Price is required!'),
  });

  return (
    <Formik
      initialValues={{
        name: '',
        description: '',
        stock: '',
        image: null,
        price: ''
      }}
      validationSchema={validation}
      onSubmit={(values) => {
        const data = new FormData();
        data.append('name', values.name);
        data.append('description', values.description);
        data.append('stock', values.stock);
        data.append('image', values.image);
        data.append('price', values.price);
        productCreate(data);
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form onSubmit={handleSubmit}>
          <Card>
            <CardHeader title="Add Product" />
            <Divider />
            <CardContent>
              <div>
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.name && errors.name)}
                  helperText={touched.name && errors.name}
                  label="Product Name"
                  name="name"
                  onChange={handleChange}
                  type="name"
                  onBlur={handleBlur}
                  value={values.name}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Stock"
                  name="stock"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="number"
                  value={values.stock}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.price && errors.price)}
                  helperText={touched.price && errors.price}
                  label="Price"
                  name="price"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="number"
                  value={values.price}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15, width: 230, }}
                  accept="image/*"
                  id="image"
                  name="image"
                  onBlur={handleBlur}
                  onChange={(event) => {
                    setFieldValue('image', event.currentTarget.files[0]);
                  }}
                  type="file"
                  placeholder="none"
                  variant="outlined"
                  hidden
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.description && errors.description)}
                  helperText={touched.description && errors.description}
                  label="Description"
                  name="description"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.description}
                  variant="outlined"
                />
                <Button
                  style={{ padding: 15, margin: 15 }}
                  color="primary"
                  type="submit"
                  variant="contained"
                  enabled={isSubmitting}
                >
                  Add Product
                </Button>
              </div>
            </CardContent>
            <Divider />
          </Card>
        </form>
      )}
    </Formik>
  );
};

export default AddForm;
