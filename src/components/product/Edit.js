import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { productEdit } from '../../utils/promiseMethod';

const EditForm = ({ handleCancel, products }) => {
  const validation = Yup.object().shape({
    name: Yup.string().max(255).required('Name is required!'),
    description: Yup.string().max(255).required('Description is required!'),
    price: Yup.string().max(255).required('Price is required!')
  });

  return (
    <Formik
      initialValues={{
        goods_id: '' || products.id,
        name: '' || products.name,
        description: '' || products.description,
        stock: '' || products.stock,
        image: null,
        price: '' || products.price
      }}
      validationSchema={validation}
      onSubmit={(values) => {
        const data = new FormData();
        data.append('goods_id', values.goods_id);
        data.append('name', values.name);
        data.append('description', values.description);
        data.append('stock', values.stock);
        data.append('image', values.image);
        data.append('price', values.price);
        productEdit(data);
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form onSubmit={handleSubmit}>
          <Card>
            <CardHeader title="Edit Product" />
            <Divider />
            <CardContent>
              <div>
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.name && errors.name)}
                  helperText={touched.name && errors.name}
                  label="Product ID"
                  name="goods_id"
                  onChange={handleChange}
                  type="number"
                  onBlur={handleBlur}
                  value={values.goods_id}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.name && errors.name)}
                  helperText={touched.name && errors.name}
                  label="Product Name"
                  name="name"
                  onChange={handleChange}
                  type="name"
                  onBlur={handleBlur}
                  value={values.name}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.description && errors.description)}
                  helperText={touched.description && errors.description}
                  label="Description"
                  name="description"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.description}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Stock"
                  name="stock"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="number"
                  value={values.stock}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15, width: 230 }}
                  accept="image/*"
                  id="image"
                  name="image"
                  onBlur={handleBlur}
                  onChange={(event) => {
                    setFieldValue('image', event.currentTarget.files[0]);
                  }}
                  type="file"
                  placeholder="none"
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.price && errors.price)}
                  helperText={touched.price && errors.price}
                  label="Price"
                  name="price"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="number"
                  value={values.price}
                  variant="outlined"
                />
                <Button
                  style={{ padding: 15, margin: 15 }}
                  color="primary"
                  type="submit"
                  variant="contained"
                  enabled={isSubmitting}
                >
                  Save Product
                </Button>
                <Button
                  style={{ padding: 15 }}
                  color="primary"
                  variant="contained"
                  onClick={handleCancel}
                >
                  Cancel
                </Button>
              </div>
            </CardContent>
            <Divider />
          </Card>
        </form>
      )}
    </Formik>
  );
};
EditForm.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  products: PropTypes.object.isRequired
};

export default EditForm;
