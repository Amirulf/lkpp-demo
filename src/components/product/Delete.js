import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { productDelete } from '../../utils/promiseMethod';

const EditForm = ({ open, onClose, products }) => {
  const validation = Yup.object().shape({
    goods_id: Yup.string().max(255).required('Product ID is required!')
  });

  return (
    <Dialog onClose={onClose} open={open} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Edit Produk</DialogTitle>
      <Formik
        initialValues={{
          goods_id: products.id || ''
        }}
        validationSchema={validation}
        onSubmit={(values, { setLoading }) => {
          productDelete(values);
          setLoading(false);
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          touched,
          values
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent dividers>
              <TextField
                style={{ margin: 15 }}
                error={Boolean(touched.name && errors.name)}
                helperText={touched.name && errors.name}
                label="Product ID"
                name="goods_id"
                onChange={handleChange}
                type="number"
                onBlur={handleBlur}
                value={values.goods_id}
                variant="outlined"
              />
              <Button
                style={{ padding: 15, margin: 10 }}
                color="primary"
                type="submit"
                variant="contained"
              >
                Delete Product
              </Button>
              <Button onClick={onClose} color="primary">
                Cancel
              </Button>
            </DialogContent>
          </form>
        )}
      </Formik>
    </Dialog>
  );
};

EditForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  products: PropTypes.func.isRequired
};
export default EditForm;
