import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Dialog,
  DialogActions,
  useMediaQuery,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    minWidth: 345,
  },
  media: {
    height: 340,
  },
});

const Detail = ({ open, onClose, itemDetails }) => {
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={onClose}
      aria-labelledby="responsive-dialog-title"
    >
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={itemDetails.image}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h3" component="h2">
              {itemDetails.name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {itemDetails.description}
            </Typography>
            <Typography>
              <span>{Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR', maximumSignificantDigits: 3 }).format(itemDetails.price)}</span>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
      <DialogActions>
        <Button onClick={onClose} color="primary" autoFocus>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

Detail.propTypes = {
  itemDetails: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

export default Detail;
