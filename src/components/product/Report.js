import {
  Button,
  Dialog,
  TextField,
  Card,
  Divider,
  CardHeader,
  CardContent
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';

const EditForm = ({ open, onClosed, reports }) => {
  const validation = Yup.object().shape({
    goods_id: Yup.string().max(255).required('Product ID is required!')
  });

  return (
    <Dialog onClose={onClosed} open={open} aria-labelledby="form-dialog-title">
      <Formik
        initialValues={{
          customer_id: '',
          customer_name: '',
          'goods_details.sku': reports.sku || '',
          'goods_details.name': reports.name || '',
          'goods_details.amount': '',
          'goods_details.price': reports.price || '',
          sales_code: ''
        }}
        validationSchema={validation}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          touched,
          values
        }) => (
          <form onSubmit={handleSubmit}>
            <Card>
              <CardHeader title="Add Product to Report" />
              <Divider />
              <CardContent>
                <div>
                  <TextField
                    style={{ margin: 15 }}
                    error={Boolean(touched.customer_id && errors.customer_id)}
                    helperText={touched.customer_id && errors.customer_id}
                    label="Customer ID"
                    name="customer_id"
                    onChange={handleChange}
                    type="name"
                    onBlur={handleBlur}
                    value={values.customer_id}
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="Customer Name"
                    name="customer_name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="name"
                    value={values.customer_name}
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="SKU"
                    name="['goods_details.sku']"
                    value={values['goods_details.sku']}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="number"
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="Product Name"
                    name="['goods_details.name']"
                    value={values['goods_details.name']}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="Amount"
                    name="['goods_details.amount']"
                    value={values['goods_details.amount']}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="Price"
                    name="['goods_details.price']"
                    value={values['goods_details.price']}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    variant="outlined"
                  />
                  <TextField
                    style={{ margin: 15 }}
                    label="Sales Code"
                    name="sales_code"
                    value={values.sales_code}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    variant="outlined"
                  />
                  <Button
                    style={{ padding: 15, margin: 15 }}
                    color="primary"
                    type="submit"
                    variant="contained"
                  >
                    Add Report
                  </Button>
                  <Button onClick={onClosed} color="primary">
                    Cancel
                  </Button>
                </div>
              </CardContent>
              <Divider />
            </Card>
          </form>
        )}
      </Formik>
    </Dialog>
  );
};

EditForm.propTypes = {
  onClosed: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  reports: PropTypes.func.isRequired
};
export default EditForm;
