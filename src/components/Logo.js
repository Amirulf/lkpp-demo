import LogoIcon from '../assets/logo.png';

const Logo = (props) => <img width="50px" alt="Logo" src={LogoIcon} {...props} />;

export default Logo;
