import { useState, useEffect, useRef } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  LinearProgress,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { getAllProduct } from '../../utils/promiseMethod';
import Add from './Add';

const Product = ({ ...rest }) => {
  const myRef = useRef(null);
  const [items, setItems] = useState([]);
  const [limit, setLimit] = useState(10);
  const [rowPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [isLoading, setLoading] = useState(true);
  const [show, setShow] = useState(false);
  const [productDetail, setProductDetail] = useState('');

  useEffect(async () => {
    const result = await getAllProduct();
    setItems(result.data.data);
    setLoading(false);
  }, []);

  const handleHide = () => {
    setShow(false);
  };
  const handleAddReport = (item) => {
    setShow(true);
    setProductDetail(item);
    myRef.current.scrollIntoView();
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowPerPage - Math.min(rowPerPage, items.length - page * rowPerPage);
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Box style={{ marginTop: -50 }}>
      <div ref={myRef}>
        {!show ? (
          <></>
        ) : (<Add products={productDetail} handleCancel={() => handleHide()} />)}
        <br />
      </div>
      {isLoading ? (
        <LinearProgress />
      ) : (
        <Card {...rest}>
          <PerfectScrollbar>
            <Box sx={{ minWidth: 1050 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Sku</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Stock</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowPerPage > 0
                    ? items.slice(page * rowPerPage, page * rowPerPage + rowPerPage)
                    : items.data
                  ).map((item) => (
                    /* eslint no-underscore-dangle: 0 */
                    <TableRow hover key={item.id}>
                      <TableCell>{item.name}</TableCell>
                      <TableCell>{item.sku}</TableCell>
                      <TableCell>
                        {Intl.NumberFormat('id-ID', {
                          style: 'currency',
                          currency: 'IDR',
                          maximumSignificantDigits: 3
                        }).format(item.price)}
                      </TableCell>
                      <TableCell>{`${item.stock} Pcs`}</TableCell>
                      <TableCell align="center">
                        <Button
                          onClick={() => handleAddReport(item)}
                          variant="contained"
                          color="primary"
                          disabled={show}
                          style={{ margin: 3 }}
                        >
                          <AddIcon />
                          <small>Add To Report</small>
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <TablePagination
            component="div"
            count={items.length}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[10, 15, 25]}
          />
        </Card>
      )}
    </Box>
  );
};

export default Product;
