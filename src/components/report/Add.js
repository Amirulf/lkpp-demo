import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { createReport } from '../../utils/promiseMethod';

const AddSales = ({ products, handleCancel }) => {
  const validation = Yup.object().shape({
    customer_id: Yup.string().max(255).required('Customer ID is required!'),
  });

  return (
    <Formik
      initialValues={{
        customer_id: '',
        customer_name: '',
        goods_details: [{
          sku: products.sku || '',
          name: products.name || '',
          amount: '',
          price: products.price || ''
        }],
        sales_code: ''
      }}
      validationSchema={validation}
      onSubmit={(values) => {
        createReport(values);
        console.log(values);
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
      }) => (
        <form onSubmit={handleSubmit}>
          <Card>
            <CardHeader title="Add Report" />
            <Divider />
            <CardContent>
              <div>
                <TextField
                  style={{ margin: 15 }}
                  error={Boolean(touched.customer_id && errors.customer_id)}
                  helperText={touched.customer_id && errors.customer_id}
                  label="Customer ID"
                  name="customer_id"
                  onChange={handleChange}
                  type="name"
                  onBlur={handleBlur}
                  value={values.customer_id}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Customer Name"
                  name="customer_name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="name"
                  value={values.customer_name}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="SKU"
                  name="goods_details[0].sku"
                  value={values.goods_details[0].sku}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="number"
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Product Name"
                  name="goods_details[0].name"
                  value={values.goods_details[0].name}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Amount"
                  name="goods_details[0].amount"
                  value={values.goods_details[0].amount}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Price"
                  name="goods_details[0].price"
                  value={values.goods_details[0].price}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                />
                <TextField
                  style={{ margin: 15 }}
                  label="Sales Code"
                  name="sales_code"
                  value={values.sales_code}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                />
                <Button
                  style={{ padding: 15, margin: 15 }}
                  color="primary"
                  type="submit"
                  enabled={isSubmitting}
                  variant="contained"
                >
                  Add Report
                </Button>
                <Button
                  style={{ padding: 15 }}
                  color="primary"
                  variant="contained"
                  onClick={handleCancel}
                >
                  Cancel
                </Button>
              </div>
            </CardContent>
            <Divider />
          </Card>
        </form>
      )}
    </Formik>
  );
};
AddSales.propTypes = {
  products: PropTypes.object.isRequired,
  handleCancel: PropTypes.func.isRequired,
};

export default AddSales;
