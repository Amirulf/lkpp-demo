import { Navigate } from 'react-router-dom';
import DashboardLayout from './components/DashboardLayout';
import MainLayout from './components/MainLayout';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import LoginAdmin from './pages/LoginAdmin';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Sales from './pages/Sales';
import Reporting from './pages/Report';
import History from './pages/History';
import Customer from './pages/Customer';
import Product from './pages/Product';

const routes = (isLoggedIn) => [
  {
    path: 'app',
    element: isLoggedIn ? <DashboardLayout /> : <Navigate to="/login" />,
    children: [
      { path: 'dashboard', element: <Dashboard /> },
      { path: 'sales', element: <Sales /> },
      { path: 'product', element: <Product /> },
      { path: 'admin', element: <Customer /> },
      { path: 'report', element: <Reporting /> },
      { path: 'report/history', element: <History /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: '/',
    element: !isLoggedIn ? <MainLayout /> : <Navigate to="/app/dashboard" />,
    children: [
      { path: 'login', element: <Login /> },
      { path: 'login-admin', element: <LoginAdmin /> },
      { path: 'register', element: <Register /> },
      { path: '404', element: <NotFound /> },
      { path: '/', element: <Navigate to="/app/dashboard" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
